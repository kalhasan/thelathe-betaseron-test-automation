package com.test.hcp;

import java.io.IOException;

import org.testng.annotations.Test;



public class TestHCP extends com.base.AbstractClass{

	LocatorHCP hl = new LocatorHCP();
	@Test
	public void HCPTest() throws IOException{
		openChromeBrowser();
		//openFirefoxBrowser();
		getURL("https://betaseron.com/wrong");
		implicitWait();
		//clickByCss("#hcp-link>span");
		String  title = returnTitle();
		System.out.println(title);
		implicitWait();
		takeScreenShot(title);
		clickByCss("#hcp-link>span");
		title=returnTitle();
		takeScreenShot(title);
	}
}
