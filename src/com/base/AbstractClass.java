package com.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AbstractClass {
	
	WebDriver driver;
	
	//Browser Openers
	public void openChromeBrowser(){
		System.setProperty("webdriver.chrome.driver",
				"/Users/kalhasan/Desktop/chromedriver");
		driver = new ChromeDriver();
		//driver.get("https://www.betaseron.com");
	}
	public void openFirefoxBrowser(){
		driver = new FirefoxDriver();
	}
	
	// Get methods and Navigators
	public void getURL(String url){
		driver.get(url);
	}
	public void navigateTo(String Url){
		driver.navigate().to(Url);
	}
	
	// Click it Baby
	
	public void clickByCss(String css){
		driver.findElement(By.cssSelector(css)).click();
	}
	
	public void clickByID(String id){
		driver.findElement(By.id(id)).click();
	}
	
	public void clickByName(String name){
		driver.findElement(By.name(name)).click();
	}
	
	public void clickByXpath(String xpath){
		driver.findElement(By.xpath(xpath)).click();
	}

	// Type 
	public void typeByXpath(String xpath, String value){
		driver.findElement(By.name(xpath)).sendKeys(value);;
	}
	
	public void typeByName(String name, String value){
		driver.findElement(By.name(name)).sendKeys(value);;
	}
	
	// Wait Commands
	
	public void implicitWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
	}
	public void waitForPageToload(long time){
		
		driver.manage().timeouts().setScriptTimeout(time, TimeUnit.SECONDS);
	}
}
