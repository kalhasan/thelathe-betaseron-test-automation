package com.test.hcp;

public class HCPLocator {
	public String hcpLink_Xpath = "//*[@id='hcp-link']/span";
	public String pageAccessCancel_Xpath="";
	public String pageAccessConfirm_Xpath ="";
	public String formFirstName_name = "firstName";
	public String formLastName_name = "lastName";
	public String formCity_name = "city";
	public String formState_Xpath =".//*[@id='login_info']/div[4]/div[2]/div/div/div[2]";
	public String formZipCode_name= "zipCode";
	public String formPhoneNumber_name = "phone";
	public String formStateLicense_name = "license";
	public String formEmail_name = "email";
	public String formConfirmEmail_name = "confirmEmail";
	public String buttonSubmint_Id = "register_button";
}
